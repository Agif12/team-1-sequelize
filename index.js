const express = require('express');
const fileUpload = require('express-fileupload');

const customers = require('./routes/customers');
const expeditions = require('./routes/expeditions');
const goods = require('./routes/goods');
const suppliers = require('./routes/suppliers');
const transactions = require('./routes/transactions');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ urlencoded: true }));

app.use(fileUpload());

app.use(express.static('public'));

app.use('/customers', customers);
app.use('/expeditions', expeditions);
app.use('/goods', goods);
app.use('/suppliers', suppliers);
app.use('/transactions', transactions);

const port = process.env.port || 3000;
app.listen(port, () => console.log(`Server running on port ${port}...`))
