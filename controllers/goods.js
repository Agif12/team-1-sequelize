const { good, supplier } = require('../models');

class Good {
  async createGoods(req, res, next) {
    try {
      const { image } = req.files
      const newData = await good.create(req.body);

      const data = await good.findOne({
        where: {
          id: newData.id,
        },
        attributes: {
          exclude: [
            'createdAt',
            'updatedAt',
            'deletedAt']
        },
        include: [
          {
            model: supplier,
            attributes: ['name']
          },
        ],
      });

      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ['Internal Server Error'] });
    }
  }

  async getAllGoods(req, res, next) {
    try {

      const data = await good.findAll({
        where: {

        },
        attributes: {
          exclude: [
            'createdAt',
            'updatedAt',
            'deletedAt']
        },
        include: [
          {
            model: supplier,
            attributes: ['name']
          },
        ],
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ['Internal Server Error'] });
    }
  }

  async getOneGoods(req, res, next) {
    try {

      const { id } = req.params
      const data = await good.findOne({
        where: {
          id: id,
        },
        attributes: {
          exclude: [
            'createdAt',
            'updatedAt',
            'deletedAt']
        },
        include: [
          {
            model: supplier,
            attributes: ['name']
          },
        ],
      });

      if (data === null) {
        return res.status(404).json({ errors: ['Good not found'] });
      }

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ['Internal Server Error'] });
    }
  }

  async updateGoods(req, res, next) {
    try {
      const { id } = req.params

      const updatedData = await good.update(req.body, {
        where: {
          id: id,
        },
      })

      if (updatedData[0] === 0) {
        return res.status(404).json({ errors: ["Goods not found"] });
      }

      const data = await good.findOne({
        where: {
          id: id,
        },
        attributes: {
          exclude: [
            'createdAt',
            'updatedAt',
            'deletedAt']
        },
        include: [
          {
            model: supplier,
            attributes: ['name']
          },
        ],
      });

      if (data === null) {
        return res.status(404).json({ errors: ['Good not found'] });
      }

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ['Internal Server Error'] });
    }
  }

  async deleteGoods(req, res, next) {
    try {

      const { id } = req.params
      const deletedData = await good.destroy({
        where: {
          id: id,
        },
      });

      if (!deletedData) {
        return res.status(404).json({ errors: ['Good not found or Has been deleted'] });
      }

      res.status(201).json({ message: ['Success Deleting Data'] });
    } catch (error) {
      res.status(500).json({ errors: ['Internal Server Error'] });
    }
  }
}

module.exports = new Good();