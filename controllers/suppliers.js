const { supplier } = require('../models');

class Suppliers {
    async getAllSuppliers(req, res, next) {
        try {
            let data = await supplier.findAll({
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'deletedAt'],
                },
            });

            if (data.length === 0) {
                return res.status(404).json({ errors: 'Suppliers not found' });
            }

            res.status(200).json({ data });

        } catch (error) {
            console.log(error);
            res.status(500).json({ error: 'Internal Server Error All Supplier' });
        }
    }

    async getDetailSupplier(req, res, next) {
        try {
            let data = await supplier.findOne({
                where: { id: req.params.id },
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'deletedAt'],
                },
            });

            if (!data) {
                return res.status(404).json({ errors: 'Supplier not found' });
            }

            res.status(200).json({ data })
        } catch (error) {
            res.status(500).json({ errors: 'Internal Server Error Detail Supplier' });
        }
    }

    async createSupplier(req, res, next) {
        try {
            const newData = await supplier.create(req.body);

            const data = await supplier.findOne({
                where: { id: newData.id },
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'deletedAt'],
                },
            });

            res.status(201).json({ data });
        } catch (error) {
            res.status(500).json({ errors: 'Internal Server Error Create Supplier' });
        }
    }

    async updateSupplier(req, res, next) {
        try {
            const updatedData = await supplier.update(req.body, {
                where: { id: req.params.id }
            });

            if (updatedData[0] === 0) {
                return res.status(404).json({ errors: 'Supplier not found' });
            }

            const data = await supplier.findOne({
                where: { id: req.params.id },
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'deletedAt'],
                },
            });

            res.status(201).json({ data })
        } catch (error) {
            res.status(500).json({ errors: 'Internal Server Error Update Supplier' });
        }
    }

    async deleteSupplier(req, res, next) {
        try {
            let data = await supplier.destroy({
                where: { id: req.params.id }
            });

            if (!data) {
                return res.status(404).json({ errors: 'Supplier not found' })
            }

            res.status(200).json({ message: 'Success delete supplier' });
        } catch (error) {
            res.status(500).json({ errors: 'Internal Server Error Delete Supplier' })
        }
    }
}

module.exports = new Suppliers();

