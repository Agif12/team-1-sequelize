const { expedition } = require('../models');

class Expeditions {
    async getAllExpeditions(req, res, next) {
        try {

            let data = await expedition.findAll({
                attributes: {
                    exclude: ["createdAt", "updatedAt", "deletedAt"],
                },
            });

            if (data.length === 0) {
                return res.status(404).json({ errors: 'Expeditions not found' });
            }

            res.status(200).json({ data });

        } catch (error) {
            console.log(error);
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    async getDetailExpediton(req, res, next) {
        try {
            let data = await expedition.findOne({
                where: { id: req.params.id },
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'deletedAt'],
                },
            });

            if (!data) {
                return res.status(404).json({ errors: 'Expeditions not found' });
            }

            res.status(200).json({ data });
        } catch (error) {
            res.status(500).json({ errors: 'Internal Server Error' });
        }
    }

    async createExpedition(req, res, next) {
        try {
            const newData = await expedition.create(req.body);

            const data = await expedition.findOne({
                where: { id: newData.id },
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'deletedAt'],
                },
            });

            res.status(201).json({ data });

        } catch (error) {
            res.status(500).json({ errors: 'Internal Server Error' });
        }
    }

    async updateExpedition(req, res, next) {
        try {
            const updatedData = await expedition.update(req.body, {
                where: { id: req.params.id }
            });

            if (updatedData[0] === 0) {
                return res.status(404).json({ errors: 'Expedition not found' });
            }

            const data = await expedition.findOne({
                where: { id: req.params.id },
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'deletedAt'],
                },
            });

            res.status(201).json({ data });
        } catch (error) {
            res.status(500).json({ errors: 'Internal Server Error' });
        }
    }

    async deleteExpedition(req, res, next) {
        try {
            let data = await expedition.destroy({
                where: { id: req.params.id }
            });

            if (!data) {
                return res.status(404).json({ errors: 'Expedition not found' });
            }

            res.status(200).json({ message: 'Success delete transaction' });
        } catch (error) {
            res.status(500).json({ errors: 'Internal Server Error' });
        }
    }
}

module.exports = new Expeditions();