const { customer } = require("../models");

class Customer {
  async getAllCustomer(req, res, next) {
    try {
      let data = await customer.findAll({
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["Customer not found"] })
      }

      res.status(200).json({ data });
    } catch (error) {
      // console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  };

  async getDetailCustomer(req, res, next) {
    try {
      let { id } = req.params;

      const data = await customer.findOne({
        where: { id: id },
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      });

      if (data === null) {
        return res.status(404).json({ errors: ["Costumers not found"] });
      }
      
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  };

  async createCustomer(req, res, next) {
    try {
      const newData = await customer.create(req.body);

      const data = await customer.findOne({
        where: {
          id: newData.id,
        },
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      });

      res.status(201).json({ data });
    } catch (error) {
      // console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  };

  async updateCustomer(req, res, next) {
    try {
      const { id } = req.params;

      const updateData = await customer.update(req.body, {
        where: { id: id },
      });
      // If no data updated
      if (updateData[0] === 0) {
        return res.status(404).json({ errors: ["Customer not found"] });
      }
      const data = await customer.findOne({
        where: {
          id: id,
        },
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      });

      res.status(201).json({ data });
    } catch (error) {
      // console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  };

  async deleteCustomer(req, res, next) {
    try {
      const { id } = req.params;
      const deletedData = await customer.destroy({
        where: {
          id: id,
        },
      });

      if (deletedData.id !== id) {
        return res
          .status(404)
          .json({ errors: ["Customer or Has been deleted"] });
      }

      res.status(200).json({ message: ["Success deleting data"] });
    } catch (error) {
      // console.log(error);
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
};

module.exports = new Customer();
