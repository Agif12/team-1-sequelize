'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class expedition extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.expedition.hasMany(models.transaction, {
        foreignKey: 'id_expedition',
      });
    }
  };
  expedition.init({
    name: DataTypes.STRING,
    image: DataTypes.STRING
  }, {
    sequelize,
    paranoid: true,
    timestamps: true,
    modelName: 'expedition',
  });
  return expedition;
};