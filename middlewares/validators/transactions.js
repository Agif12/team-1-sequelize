const validator = require('validator');
const { good } = require('../../models');

exports.createOrUpdateTransactionValidator = async (req, res, next) => {
    try {
        const errors = [];

        if (!validator.isInt(req.body.id_good)) {
            errors.push(`ID Good must be number (integer)`);
        }

        if (!validator.isInt(req.body.id_customer)) {
            errors.push(`ID Customer must be number (integer)`);
        }

        if (!validator.isInt(req.body.id_expedition)) {
            errors.push(`ID Customer must be number (integer)`);
        }

        if (!validator.isInt(req.body.quantity)) {
            errors.push(`Quantity must be number (integer)`);
        }

        if (errors.length > 0) {
            return res.status(400).json({ errors: errors });
        }

        const findGood = await good.findOne({ where: { id: req.body.id_good } });

        if (!findGood) {
            return res.status(404).json({ error: ['Good not found'] })
        }

        const { price } = findGood;
        req.body.total = parseFloat(price) * parseFloat(req.body.quantity);

        next();

    } catch (error) {
        console.log(error);
        res.status(500).json({ errors: 'Internal Server Errors' });
    }
}