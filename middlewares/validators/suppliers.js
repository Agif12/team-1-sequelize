const path = require("path");
const crypto = require("crypto");
const validator = require("validator");
const { promisify } = require("util");

exports.createSupplierValidator = async (req, res, next) => {
    try {
        const errors = [];

        if (validator.isEmpty(req.body.name)) {
            errors.push('Name must not be empty');
        }

        if (errors.length > 0) {
            return res.status(400).json({ errors: errors });
        }

        // If image was uploaded
        if (req.files.image) {
            // req.files.image is come from key (file) in postman
            const file = req.files.image;

            // Make sure image is photo
            if (!file.mimetype.startsWith("image/")) {
                errors.push("File must be an image");
            }

            // Check file size (max 10MB)
            if (file.size > 10000000) {
                errors.push("Image must be less than 1MB");
            }

            // If error
            if (errors.length > 0) {
                return res.status(400).json({ errors: errors });
            }

            // Create custom filename
            let fileName = crypto.randomBytes(16).toString("hex");

            // Rename the file
            file.name = `${fileName}${path.parse(file.name).ext}`;

            // Make file.mv to promise
            const move = promisify(file.mv);

            // Upload image to /public/images
            await move(`./public/images/suppliers/${file.name}`);

            // assign req.body.image with file.name
            req.body.image = file.name;
        }
        if (!(req.files && req.files.image)) {
            req.body.image = noimage;
        }
        next();
    } catch (error) {
        console.log(error);
        res.status(500).json({ errors: ["Internal Server Error"] });
    }
};
