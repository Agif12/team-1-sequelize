const express = require('express');

const { createExpeditionValidator } = require('../middlewares/validators/expeditions');
const {
    createExpedition,
    getAllExpeditions,
    getDetailExpediton,
    updateExpedition,
    deleteExpedition } = require('../controllers/expeditions');

const router = express.Router();

router.get('/', getAllExpeditions);
router.get('/:id', getDetailExpediton);
router.post('/', createExpeditionValidator, createExpedition);
router.put('/:id', createExpeditionValidator, updateExpedition);
router.delete('/:id', deleteExpedition);

module.exports = router;