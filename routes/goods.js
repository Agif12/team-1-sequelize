// const express = require('express');

// const { createGoodValidator } = require('../middlewares/validators/goods');
// const { createGood } = require('../controllers/goods');

// const router = express.Router();

// router.post('/', createGoodValidator, createGood);

// module.exports = router;

const express = require('express');

const {
    createGoods,
    getAllGoods,
    getOneGoods,
    updateGoods,
    deleteGoods,
} = require('../controllers/goods');

const {
  createAndUpdateGoodValidator,
} = require("../middlewares/validators/goods");

const router = express.Router();

router.get('/', getAllGoods);
router.get('/:id', getOneGoods);
router.post("/", createAndUpdateGoodValidator, createGoods);
router.put("/:id", createAndUpdateGoodValidator, updateGoods);
router.delete('/:id', deleteGoods);

module.exports = router;