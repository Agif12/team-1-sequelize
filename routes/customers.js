const express = require("express");

const {
  createCustomer,
  getAllCustomer,
  getDetailCustomer,
  updateCustomer,
  deleteCustomer,
} = require("../controllers/customers");

const {createAndUpdateCustomerValidator} = require("../middlewares/validators/customers");

const router = express.Router();

router.get("/", getAllCustomer);
router.get("/:id", getDetailCustomer);
router.post("/", createAndUpdateCustomerValidator, createCustomer);
router.put("/:id", createAndUpdateCustomerValidator, updateCustomer);
router.delete("/:id", deleteCustomer);

module.exports = router;
