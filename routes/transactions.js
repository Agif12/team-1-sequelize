const express = require('express');

const { createOrUpdateTransactionValidator } = require('../middlewares/validators/transactions');
const { createTransaction, getAllTransactions, getDetailTransaction, updateTransaction, deleteTransaction } = require('../controllers/transactions');

const router = express.Router();

router.get('/', getAllTransactions);
router.get('/:id', getDetailTransaction);
router.post('/', createOrUpdateTransactionValidator, createTransaction);
router.put('/:id', createOrUpdateTransactionValidator, updateTransaction);
router.delete('/:id', deleteTransaction);

module.exports = router;