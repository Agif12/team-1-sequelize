const express = require('express');

const { createSupplierValidator } = require('../middlewares/validators/suppliers');
const {
    createSupplier,
    getAllSuppliers,
    getDetailSupplier,
    updateSupplier,
    deleteSupplier } = require('../controllers/suppliers');

const router = express.Router();

router.get('/', getAllSuppliers);
router.get('/:id', getDetailSupplier);
router.post('/', createSupplierValidator, createSupplier);
router.put('/:id', createSupplierValidator, updateSupplier);
router.delete('/:id', deleteSupplier);

module.exports = router;

